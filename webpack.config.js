const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = () => ({
  entries: {},
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: path.resolve(__dirname, 'assets', 'iep.gif'),
        to: 'plugins/iep/[name][ext]'
      }]
    })
  ],
  rules: []
})
