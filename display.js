import iepLoaderPromise from 'instrumenpoche'
import { addElementAfter } from 'sesajstools/dom'
import log from 'sesajstools/utils/log'
import page from 'client/page'

/**
 * Affiche une ressource iep (animation instrumenpoche)
 * @service plugins/iep/display
 * @param {Ressource}      ressource  L'objet ressource (une ressource iep a en parametres soit une propriété url
 *                                      avec l'url du xml soit une propriété xml avec la string xml)
 * @param {displayOptions} options    Les options après init
 * @param {errorCallback}  next       La fct à appeler quand l'iep sera chargé (sans argument ou avec une erreur)
 */
export function display (ressource, options, next) {
  let boutonVu
  const container = options.container
  if (!next) {
    next = (error) => {
      if (error) page.addError(error)
    }
  }

  try {
    log('start iep display avec la ressource', ressource)
    if (!container) return next(Error('Il faut passer dans les options un conteneur html pour afficher cette ressource'))
    // les params minimaux
    if (!ressource.oid || !ressource.titre || !ressource.parametres) return next(Error('Paramètres manquants'))
    const { url, xml } = ressource.parametres
    if (!xml && !url) return next(Error('Script instrumenpoche manquant'))

    const script = xml || url
    const iepOptions = {
      zoom: true
    }
    iepLoaderPromise(container, script, iepOptions)
      .then((/* iepApp qu'il ne faut surtout pas filer à next en 1er argument */) => {
        // On peut ajouter l'envoi du résultat
        if (options.resultatCallback) {
          // le résultat est toujours le même
          const resultat = {
            fin: true,
            score: 1,
            deferSync: true
          }
          let isResultatSend = false
          // au clic sur bouton vu on envoie
          boutonVu = page.addBoutonVu(function () {
            isResultatSend = true
            options.resultatCallback(resultat)
          })
          // pas réussi à pouvoir cliquer sur ce @#! bouton sans décaler le reste (z-index suffit pas…)
          const titre = window.document.getElementById('titre')
          if (titre && titre.style === 'none') {
            addElementAfter(boutonVu, 'hr', { style: 'visibility: hidden; clear: both' })
          }
          // mais aussi à la fermeture de la fenêtre, si on a pas cliqué sur Vu avant
          container.addEventListener('unload', function () {
            if (!isResultatSend) options.resultatCallback(resultat)
          })
        }

        next()
      })
      .catch(next)
  } catch (error) {
    next(error)
  }
}
