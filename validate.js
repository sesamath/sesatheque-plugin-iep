const xmlParser = new DOMParser()

const validate = ({ parametres: { url, xml } }, errors) => {
  if (!url && !xml) {
    errors.parametres = errors.parametres || {}
    errors.parametres.xml = errors.parametres.url = 'Il faut renseigner l’un de ces deux champs'
    return
  }
  if (url && !/https?:\/\//.test(url)) {
    errors.parametres = errors.parametres || {}
    errors.parametres.url = 'L’adresse doit commencer par http:// ou https://'
  }
  if (xml) {
    const xmlDOM = xmlParser.parseFromString(xml, 'text/xml')
    const error = xmlDOM.querySelector('parsererror')
    if (error) {
      const messageContainer = error.querySelector('div')
      const message = messageContainer ? messageContainer.innerText : 'cf ci-dessus (erreurs signalées dans la marge)'
      errors.parametres = errors.parametres || {}
      errors.parametres.xml = `Ce champ doit contenir du xml valide : ${message}`
    } else if (xmlDOM.documentElement.nodeName !== 'INSTRUMENPOCHE') {
      errors.parametres = errors.parametres || {}
      errors.parametres.xml = 'Ce champ doit contenir du xml dont la racine est <INSTRUMENPOCHE>'
    }
  }
}

export default validate
